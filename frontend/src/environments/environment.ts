// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
//let server = "34.215.130.4:8107";
let server = "localhost:8080";
export const environment = {
  production: false,
  authApi: `http://${server}/auth`,
  regApi: `http://${server}/reg`,
  profileApi: `http://${server}/profile`,
  tprofileApi: `http://${server}/tprofile`,
  userApi: `http://${server}/users`,
  teacherApi: `http://${server}/teachers`,
  studentApi: `http://${server}/students`,
  activityApi: `http://${server}/activities`,
  enrolApi: `http://${server}/enrols`,
  uploadApi: `http://${server}/uploadFile`,
  commentApi: `http://${server}/comments`
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
