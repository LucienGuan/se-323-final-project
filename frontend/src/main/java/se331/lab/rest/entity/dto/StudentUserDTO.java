package se331.lab.rest.entity.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.User;

import java.util.Date;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentUserDTO {
  Long id;
  String email;
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  String pass;
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  boolean enabled;
  String status;
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  Date lastPasswordResetDate;
  String type;
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  List<Authority> authorities;
  String stid;
  String dob;
  String name;
  String surname;
  String imgurl;
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  User user;
}
