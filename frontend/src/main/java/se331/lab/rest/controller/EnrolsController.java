package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.lab.rest.entity.dto.EnrolsDTO;
import se331.lab.rest.service.EnrolsService;

import java.util.stream.Collectors;

@Controller
public class EnrolsController {
  @Autowired
  EnrolsService enrolsService;
  @CrossOrigin
  @GetMapping("/enrols")
  public ResponseEntity<?> getEnrols(){
    return ResponseEntity.ok(enrolsService.getEnrols().stream()
      .map(EnrolsDTO::getEnrolsListDTO).collect(Collectors.toList()));
  }
  @CrossOrigin
  @PostMapping("/enrols")
  public ResponseEntity<?> saveEnrol(@RequestBody EnrolsDTO enrolsDTO) {
    return ResponseEntity.ok(enrolsService.save(enrolsDTO.getEnrols()));
  }
  @CrossOrigin
  @PutMapping("/enrols/{id}")
  public ResponseEntity<?> saveEnrolByPut(@PathVariable long id, @RequestBody EnrolsDTO enrolsDTO) {
    enrolsDTO.setId(id);
    return ResponseEntity.ok(enrolsService.save(enrolsDTO.getEnrols()));
  }
  @CrossOrigin
  @DeleteMapping("/enrols/{id}")
  public ResponseEntity<?> deleteEnrol(@PathVariable long id) {
    return ResponseEntity.ok(enrolsService.deleteById(id));
  }

}
