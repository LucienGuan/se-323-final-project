package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import se331.lab.rest.entity.dto.StudentDTO;
import se331.lab.rest.entity.dto.UserDTO;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;
import se331.lab.rest.security.service.UserService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {
  @Autowired
  UserService userService;
  PasswordEncoder encoder = new BCryptPasswordEncoder();
  @Autowired
  UserRepository userRepository;
  @Autowired
  AuthorityRepository authorityRepository;
  @CrossOrigin
  @GetMapping("/users")
  public ResponseEntity getAllUsers() {
    return ResponseEntity.ok(this.userService.getUsers());
  }
  @CrossOrigin
  @PostMapping("/users")
  public ResponseEntity<?> saveUser(@RequestBody UserDTO userDTO) {
    return ResponseEntity.ok(userService.save(userDTO.getUser()));
  }
  @CrossOrigin
  @PutMapping("/users/{id}")
  public ResponseEntity<?> saveUserByPut(@PathVariable long id,@RequestBody UserDTO userDTO) {
    String pass = userService.findById(userDTO.getId()).getPass();
    userDTO.setId(id);
    userDTO.setPass(pass);
    userDTO.setLastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()));
    if(userDTO.getStatus().equals("reject")){
      userDTO.setEnabled(false);
    }else {
      userDTO.setEnabled(true);
    }
    return ResponseEntity.ok(userService.save(userDTO.getUser()));
  }
}
