package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.entity.dto.StudentUserDTO;
import se331.lab.rest.entity.dto.TeacherUserDTO;
import se331.lab.rest.entity.dto.UserDTO;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;
import se331.lab.rest.security.service.UserService;
import se331.lab.rest.service.StudentService;
import se331.lab.rest.service.TeacherService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ProfileController {
  @Autowired
  UserService userService;
  @Autowired
  StudentService studentService;
  @Autowired
  TeacherService teacherService;

  PasswordEncoder encoder = new BCryptPasswordEncoder();
  @Autowired
  AuthorityRepository authorityRepository;

  @CrossOrigin
  @PutMapping("/profile/{id}")
  public ResponseEntity<?> updateStudent(@PathVariable long id, @RequestBody StudentUserDTO studentUserDTO) {
    Authority auth3 = authorityRepository.findByName(AuthorityName.ROLE_STUDENT);;
    List<Authority> authorities = new ArrayList<>();
    authorities.add(auth3);
    User user = User.builder()
      .id(id)
      .pass(encoder.encode(studentUserDTO.getPass()))
      .email(studentUserDTO.getEmail())
      .enabled(true)
      .status(studentUserDTO.getStatus())
      .type(studentUserDTO.getType())
      .authorities(authorities)
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user = userService.save(user);
    Student student = Student.builder()
      .id(id)
      .stid(studentUserDTO.getStid())
      .name(studentUserDTO.getName())
      .surname(studentUserDTO.getSurname())
      .imgurl(studentUserDTO.getImgurl())
      .dob(studentUserDTO.getDob())
      .user(user)
      .build();
    user.setAppUser(student);
    student =this.studentService.saveStudent(student);
    user = userService.save(user);
    return ResponseEntity.ok(StudentUserDTO.builder()
      .id(id)
      .email(user.getEmail())
      .type(user.getType())
      .status(user.getStatus())
      .stid(student.getStid())
      .name(student.getName())
      .surname(student.getSurname())
      .imgurl(student.getImgurl())
      .dob(student.getDob())
      .build());
  }

  @CrossOrigin
  @PutMapping("/tprofile/{id}")
  public ResponseEntity<?> updateTeacher(@PathVariable long id, @RequestBody TeacherUserDTO teacherUserDTO) {
    Authority auth2 = authorityRepository.findByName(AuthorityName.ROLE_STUDENT);;
    Authority auth3 = authorityRepository.findByName(AuthorityName.ROLE_TEACHER);;
    List<Authority> authorities = new ArrayList<>();
    authorities.add(auth2);
    authorities.add(auth3);
    User user = User.builder()
      .id(id)
      .pass(encoder.encode(teacherUserDTO.getPass()))
      .email(teacherUserDTO.getEmail())
      .enabled(true)
      .status("ok")
      .type("teacher")
      .authorities(authorities)
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user = userService.save(user);

    Teacher teacher = Teacher.builder()
      .id(id)
      .name(teacherUserDTO.getName())
      .surname(teacherUserDTO.getSurname())
      .imgurl(teacherUserDTO.getImgurl())
      .user(user)
      .build();
    user.setAppUser(teacher);
    teacher =this.teacherService.saveTeacher(teacher);
    user = userService.save(user);
    return ResponseEntity.ok(teacherUserDTO.builder()
      .id(id)
      .email(user.getEmail())
      .type(user.getType())
      .name(teacher.getName())
      .surname(teacher.getSurname())
      .imgurl(teacher.getImgurl())
      .build());

  }
}
