package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.repository.TeacherRepository;

import java.util.List;
@Repository
public class TeacherAnotherDaoImpl implements TeacherAnotherDao {
    @Autowired
    TeacherRepository teacherRepository;
    @Override
    public Teacher getTeacherByLastName(String lastName) {
        return teacherRepository.findBySurname(lastName);
    }

    @Override
    public List<Teacher> getAllTeacher() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher getTeacherById(Long id) {
        return teacherRepository.findById(id).orElse(null);
    }
}
