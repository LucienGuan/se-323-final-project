package se331.lab.rest.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se331.lab.rest.security.entity.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    List<User> findAll();
}
