package se331.lab.rest.service;

import se331.lab.rest.entity.Teacher;

import java.util.List;

public interface TeacherService {
    List<Teacher> getTeachers();
  Teacher saveTeacher(Teacher teacher);
  Teacher findById(Long id);
}
