package se331.lab.rest.service;

import se331.lab.rest.entity.Teacher;

public interface TeacherAnotherService {
    Teacher getTeacherByLastName(String lastname);
}
