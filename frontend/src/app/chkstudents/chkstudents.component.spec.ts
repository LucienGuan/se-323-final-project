import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChkstudentsComponent } from './chkstudents.component';

describe('ChkstudentsComponent', () => {
  let component: ChkstudentsComponent;
  let fixture: ComponentFixture<ChkstudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChkstudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChkstudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
