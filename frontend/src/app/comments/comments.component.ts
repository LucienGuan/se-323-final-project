import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../entity/user';
import { Activity } from '../entity/activity';
import { Comment } from '../entity/comment';
import { MatTableDataSource, MatSort } from '@angular/material';
import { IdService } from '../services/id.service';
import { ActivityService } from '../services/activity.service';
import { UserService } from '../services/user.service';
import { CommentService } from '../services/comment.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TeacherService } from '../services/teacher.service';
import { Teacher } from '../entity/teacher';
import { Student } from '../entity/student';
import { StudentService } from '../services/student.service';

class Row {
  id: number;
  name: string;
  imgurl: string;
  avatar: string;
  msg: string;
  time: string;
}

@Component({
  selector: 'app-comment',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  msg: string;
  imgurl: string;
  id: User;
  activities: Activity[];
  users: User[];
  teachers: Teacher[];
  actId: number;
  actName: string;
  students: Student[];
  comments: Comment[];
  comnt: Comment;
  comTable = new MatTableDataSource<Row>();
  @ViewChild(MatSort) sort: MatSort;
  columns: string[] = [
    'id', 'name', 'imgurl', 'avatar','msg', 'time'
  ];

  constructor(
    private idSrv: IdService,
    private activitySrv: ActivityService,
    private router: Router,
    private route: ActivatedRoute,
    private studentSrv: StudentService,
    private commentSrv: CommentService,
    private teacherSrv: TeacherService
  ) {
    this.route.params.subscribe(params => {
      this.actId = params['id'];
      this.makeRows();
      if (this.activities) {
        this.actName = this.activitySrv.get(this.actId).name;
      }
    });
    this.idSrv.obsUser().subscribe(id => {
      this.id = id;
      this.makeRows();
    });
    this.activitySrv.obs().subscribe(activities => {
      this.activities = activities;
      this.makeRows();
      if (this.actId) {
        this.actName = this.activitySrv.get(this.actId).name;
      }
    });
    this.teacherSrv.obs().subscribe(teachers => {
      this.teachers = teachers;
      this.makeRows();
    });
    this.studentSrv.obs().subscribe(students => {
      this.students = students;
      this.makeRows();
    });
    this.commentSrv.obs().subscribe(comments => {
      this.comments = comments;
      this.makeRows();
    });

  }

  makeRows() {
    if (!(this.actId && this.activities && this.students && this.teachers && this.comments)) return;
    let data: Row[] = new Array<Row>();
    for (let c of this.comments) {
      if (c.activity != this.actId) continue;
      let row: Row = {
        id:     c.id,
        name:   c.name,
        avatar: c.avatar,
        msg:    c.msg,
        imgurl: c.imgurl,
        time:   c.time,
      }
      data.push(row);
    }
    this.comTable.data = data;
  }

  addcomment() {
    let comment = new Comment();
    if (this.id.type == "teacher") {
      let t = this.teacherSrv.get(this.id.id);
      comment.name = t.name;
      comment.avatar = t.imgurl;
    } else if (this.id.type == "student") {
      let s = this.studentSrv.get(this.id.id);
      comment.name = s.name;
      comment.avatar = s.imgurl;
    }
    comment.id = 0;
    comment.activity = this.actId;
    comment.msg = this.msg;
    comment.imgurl = this.imgurl;
    comment.time = "2018-12-12";
    this.commentSrv.add(comment);
  }

  ngOnInit() {
    this.comTable.sort = this.sort;
  }

}
