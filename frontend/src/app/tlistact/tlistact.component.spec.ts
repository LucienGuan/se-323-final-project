import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TlistactComponent } from './tlistact.component';

describe('TlistactComponent', () => {
  let component: TlistactComponent;
  let fixture: ComponentFixture<TlistactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TlistactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlistactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
