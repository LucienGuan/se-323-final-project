import { ListactComponent } from './../listact/listact.component';
import { ChkstudentsComponent } from './../chkstudents/chkstudents.component';
import { AddactivityComponent } from './../addactivity/addactivity.component';
import { NgModule }  from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { UsersComponent } from '../users/users.component';
import { RegisterComponent } from '../register/register.component';
import { EnroledComponent } from '../enroled/enroled.component';
import { WaitComponent } from '../wait/wait.component';
import { ProfileComponent } from '../profile/profile.component';
import { TlistactComponent } from '../tlistact/tlistact.component';
import { EditactComponent } from '../editact/editact.component';
import { ListstuComponent } from '../liststu/liststu.component';
import { CommentsComponent } from '../comments/comments.component';
import { DelCommentsComponent } from '../del-comments/del-comments.component';
import { TprofileComponent } from '../tprofile/tprofile.component';

const routes: Routes = [
  { path: 'login',        component: LoginComponent },
  { path: 'wait',         component: WaitComponent },
  { path: 'users',        component: UsersComponent },
  { path: 'addact',       component: AddactivityComponent },
  { path: 'chkstudents',  component: ChkstudentsComponent },
  { path: 'register',     component: RegisterComponent },
  { path: 'enroled',      component: EnroledComponent },
  { path: 'listact',      component: ListactComponent },
  { path: 'profile',      component: ProfileComponent },
  { path: 'tprofile',     component: TprofileComponent },
  { path: 'delcomments',  component: DelCommentsComponent },
  { path: 'comments/:id', component: CommentsComponent },
  { path: 'tlistact',     component: TlistactComponent },
  { path: 'liststu/:id',  component: ListstuComponent},
  { path: 'editact/:id',  component: EditactComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
