export class StudentUser {
    id: number;
    email: string;
    pass: string;
    type: string;
    status: string;
    stid: string;
    name: string;
    surname: string;
    imgurl: string;
    dob: Date;
}
