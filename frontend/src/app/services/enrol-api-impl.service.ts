import { Enrol } from '../entity/enrol';
import { EnrolService } from './enrol.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnrolApiImplService extends EnrolService {
    private enrols = new BehaviorSubject<Enrol[]>(null);
    private api = environment.enrolApi;

    constructor(private http: HttpClient) {
        super();
        this.load();
    }

    load() {
        this.http.get<Enrol[]>(this.api)
        .subscribe(enrols => {
            this.enrols.next(enrols);
        });
    }

    obs(): Observable<Enrol[]> {
        return this.enrols.asObservable();
    }

    get(id: number): Enrol {
        if (this.enrols == null) return null;
        let list = this.enrols.getValue();
        return list.find((obj) => obj.id === id);
    }

    getPair(stu: number, act: number): Enrol {
        if (this.enrols == null) return null;
        let list = this.enrols.getValue();
        return list.find((obj) => obj.student == stu && obj.activity == act);
    }

    private nextId(): number {
        let list = this.enrols.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(enrol: Enrol): Enrol {
        if (this.enrols == null) return null;
        let list = this.enrols.getValue();
        enrol.id = this.nextId();
        list.push(enrol);
        this.enrols.next(list);
        this.http.post<any>(this.api, enrol)
            .subscribe(data => { this.load(); });
        return enrol;
    }

    del(id: number): Enrol {
        if (this.enrols == null) return null;
        let list = this.enrols.getValue();
        let enrol = list.find((obj) => obj.id === id);
        if (enrol === null) return null;
        let i = list.indexOf(enrol);
        list.splice(i, 1);
        this.enrols.next(list);
        this.http.delete<any>(this.api + "/" + id)
            .subscribe(data => { this.load(); });
        return enrol;
    }

    upd(enrol: Enrol): Enrol {
        if (this.enrols == null) return null;
        let list = this.enrols.getValue();
        let old = list.find((obj) => obj.id === enrol.id);
        if (old === null) return null;
        let i = list.indexOf(old);
        list[i] = enrol;
        this.enrols.next(list);
        this.http.put<any>(this.api + "/" + enrol.id, enrol)
            .subscribe(data => { this.load(); });
        return old;
    }

}

