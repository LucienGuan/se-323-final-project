import { TestBed, inject } from '@angular/core/testing';

import { CommentApiImplService } from './comment-api-impl.service';

describe('CommentApiImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommentApiImplService]
    });
  });

  it('should be created', inject([CommentApiImplService], (service: CommentApiImplService) => {
    expect(service).toBeTruthy();
  }));
});
