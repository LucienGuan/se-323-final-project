import { TestBed, inject } from '@angular/core/testing';

import { CommentFileImplService } from './comment-file-impl.service';

describe('CommentFileImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommentFileImplService]
    });
  });

  it('should be created', inject([CommentFileImplService], (service: CommentFileImplService) => {
    expect(service).toBeTruthy();
  }));
});
