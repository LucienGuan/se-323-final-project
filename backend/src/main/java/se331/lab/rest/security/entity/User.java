package se331.lab.rest.security.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Person;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "USER_DETAILS")
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "STATUS")
    @NotNull
    private String status;

    @Column(name = "TYPE")
    @NotNull
    private String type;


    @Column(name = "PASSWORD", length = 100)
    @NotNull
    @Size(min = 3, max = 100)
    @JsonProperty(access = Access.WRITE_ONLY)
    private String pass;


    @Column(name = "EMAIL", length = 50)
    @NotNull
    @Size(min = 3, max = 50)
    private String email;

    @Column(name = "ENABLED")
    @NotNull
    @JsonProperty(access = Access.WRITE_ONLY)
    private Boolean enabled;

    @Column(name = "LASTPASSWORDRESETDATE")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    @JsonProperty(access = Access.WRITE_ONLY)
    private Date lastPasswordResetDate;

    @ManyToMany(fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    @Builder.Default
    @JsonProperty(access = Access.WRITE_ONLY)
    private List<Authority> authorities = new ArrayList<>();

    @OneToOne(mappedBy = "user")
    @JsonBackReference
    Person appUser;
}
