package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.entity.*;
import se331.lab.rest.repository.*;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DataLoader implements ApplicationRunner {
  @Autowired
  StudentRepository studentRepository;
  @Autowired
  TeacherRepository teacherRepository;
  @Autowired
  ActivityRepository activityRepository;
  @Autowired
  AuthorityRepository authorityRepository;
  @Autowired
  UserRepository userRepository;
  @Autowired
  EnrolsRepository enrolsRepository;
  @Autowired
  AdminRepository adminRepository;
  @Autowired
  CommentRepository commentRepository;

  @Override
  @Transactional
  public void run(ApplicationArguments args) throws Exception {
    Student student1 = Student.builder()
      .stid("602115509 ")
      .name("Kehan ")
      .surname("Zhou")
      .imgurl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1692189249,3921987537&fm=11&gp=0.jpg")
      .dob("1997-03-24")
      .build();
    Student student2 = Student.builder()
      .stid("602115508")
      .name("Lucien")
      .surname("Guan")
      .imgurl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=231455851,3781217377&fm=26&gp=0.jpg")
      .dob("1997-12-22")
      .build();


    this.studentRepository.save(student1);
    this.studentRepository.save(student2);



    Teacher teacher1 = Teacher.builder()
      .name("Chartchai")
      .surname("Doungsa-ard")
      .imgurl("https://lh3.googleusercontent.com/cVP2spE0u1g4wCx9H_WBnGPBy0SieiEly_Qs77UNbz5lxdf2eoiDH-wBdGzwlfY081vliw=s85")
      .build();

    this.teacherRepository.save(teacher1);


    Comment c1 = Comment.builder()
      .msg("it is too tired for me")
      .time("08/02/2018")
      .activity(1)
      .avatar(teacher1.getImgurl())
      .imgurl("https://dimg04.c-ctrip.com/images/350i10000000p3nnnDB66_C_1136_640_Q60.jpg?proc=source/trip;namelogo/d_40")
      .name("Chartchai")
      .build();

    this.commentRepository.save(c1);



    Activity activity1 = Activity.builder()
      .desc("Learnning activity")
      .loc("CAMT")
      .name("Activity1")
      .when("2019-12-31")
      .time("12:00")
      .regStart("2018-12-14")
      .regEnd("2019-12-29")
      .build();

    Activity activity2 = Activity.builder()
            .desc("看春节联欢晚会")
            .loc("昆明")
            .name("春节")
            .when("2020-01-25")
            .time("12:00")
            .regStart("2020-01-25")
            .regEnd("2019-02-08")
            .build();

    Activity activity3 = Activity.builder()
            .desc("Climbing Suthep")
            .loc("Suthep")
            .name("Doi Suthep")
            .when("2019-12-31")
            .time("12:00")
            .regStart("2018-12-14")
            .regEnd("2019-12-29")
            .build();



    this.activityRepository.save(activity1);
    this.activityRepository.save(activity2);
    this.activityRepository.save(activity3);



    Enrols enrols1 = Enrols.builder()
      .activity(2)
      .student(1)
      .status("confirmed")
      .build();
    Enrols enrols2 = Enrols.builder()
      .activity(1)
      .student(2)
      .status("confirmed")
      .build();
    Enrols enrols3 = Enrols.builder()
      .activity(3)
      .student(2)
      .status("confirmed")
      .build();
    Enrols enrols4 = Enrols.builder()
      .activity(3)
      .student(1)
      .status("confirmed")
      .build();
    Enrols enrols5 = Enrols.builder()
      .activity(1)
      .student(2)
      .status("confirmed")
      .build();
    this.enrolsRepository.save(enrols1);
    this.enrolsRepository.save(enrols2);
    this.enrolsRepository.save(enrols3);
    this.enrolsRepository.save(enrols4);
    this.enrolsRepository.save(enrols5);
    activity1.setLecturer(teacher1);
    teacher1.getActivities().add(activity1);
    activity2.setLecturer(teacher1);
    teacher1.getActivities().add(activity2);
    activity3.setLecturer(teacher1);
    teacher1.getActivities().add(activity3);

    Admin admin = Admin.builder()
      .name("admin")
      .surname("admin")
      .imgurl("https://drive.google.com/uc?id=1W9VLWi53eG8y6pL25Yo68A6ig9C5F32a")
      .build();
    adminRepository.save(admin);
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    Authority auth1 = Authority.builder().name(AuthorityName.ROLE_ADMIN).build();
    Authority auth2 = Authority.builder().name(AuthorityName.ROLE_TEACHER).build();
    Authority auth3 = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
    User user1, user2, user3, user4;
    user1 = User.builder()
      .pass(encoder.encode("admin"))
      .email("admin@camt.com")
      .enabled(true)
      .status("ok")
      .type("admin")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user2 = User.builder()
      .pass(encoder.encode("dto"))
      .email("dto@camt.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();

    user3 = User.builder()
      .pass(encoder.encode("kehan"))
      .email("kehan@camt.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user4 = User.builder()
      .pass(encoder.encode("junfeng"))
      .email("junfeng@camt.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();


    authorityRepository.save(auth1);
    authorityRepository.save(auth2);
    authorityRepository.save(auth3);
    user1.getAuthorities().add(auth1);
    user1.getAuthorities().add(auth2);
    user1.getAuthorities().add(auth3);

    user2.getAuthorities().add(auth2);
    user2.getAuthorities().add(auth3);


    user3.getAuthorities().add(auth3);

    user4.getAuthorities().add(auth3);


    userRepository.save(user3);
    userRepository.save(user4);
    userRepository.save(user2);
    userRepository.save(user1);


    admin.setUser(user1);
    user1.setAppUser(admin);

    teacher1.setUser(user2);
    user2.setAppUser(teacher1);


    student1.setUser(user3);
    user3.setAppUser(student1);

    student2.setUser(user4);
    user4.setAppUser(student2);



  }


}
