package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.entity.dto.ActivityDTO;
import se331.lab.rest.entity.dto.TeacherDTO;
import se331.lab.rest.service.ActivityAnotherService;
import se331.lab.rest.service.TeacherService;

import java.util.stream.Collectors;

@Controller
@Slf4j
public class ActivityAnotherController {
    @Autowired
    ActivityAnotherService activityAnotherService;
    @Autowired
    TeacherService teacherService;
    @CrossOrigin
    @GetMapping("/activities")
    public ResponseEntity<?> getActivities(){
        log.info("get activities is called");
        return ResponseEntity.ok(activityAnotherService.getActivities().stream()
                .map(ActivityDTO::getActivityListDTO).collect(Collectors.toList()));
    }
    @CrossOrigin
    @PostMapping("/activities")
    public ResponseEntity<?> saveActivity(@RequestBody ActivityDTO activityDTO) {
        log.info("save activities is called");
        Teacher t = teacherService.findById( Long.valueOf(activityDTO.getTeacher()));
        activityDTO.setLecturer(TeacherDTO.getTeacherDTO(t));
        return ResponseEntity.ok(activityAnotherService.save(activityDTO.getActivity()));
    }
    @CrossOrigin
    @PutMapping("/activities/{id}")
    public ResponseEntity<?> saveActivityByPut(@PathVariable long id,@RequestBody ActivityDTO activityDTO) {
      activityDTO.setId(id);
      Teacher t = teacherService.findById( Long.valueOf(activityDTO.getTeacher()));
      activityDTO.setLecturer(TeacherDTO.getTeacherDTO(t));
      log.info("save activities is called");
      return ResponseEntity.ok(activityAnotherService.save(activityDTO.getActivity()));
    }
}
